<?php

/**
 * @file
 * Providing extra functionality for the Model UI via views.
 */


/**
 * Implements hook_views_data()
 */
function model_views_data_alter(&$data) { 
  $data['model']['link_model'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the model.'),
      'handler' => 'model_handler_link_field',
    ),
  );
  $data['model']['edit_model'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the model.'),
      'handler' => 'model_handler_edit_link_field',
    ),
  );
  $data['model']['delete_model'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the model.'),
      'handler' => 'model_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows models/model/%model_id/op
  $data['model']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this model.'),
      'handler' => 'model_handler_model_operations_field',
    ),
  );

  // created field
  $data['model']['created'] = array(
    'title' => t('Post date'), // The item it appears as on the UI,
    'help' => t('The date the model was posted.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // changed field
  $data['model']['changed'] = array(
    'title' => t('Changed date'), // The item it appears as on the UI,
    'help' => t('The date the model was changed.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
}


/**
 * Implements hook_views_default_views().
 */
function model_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'models';
  $view->description = 'A list of all models';
  $view->tag = 'models';
  $view->base_table = 'model';
  $view->human_name = 'Models';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Models';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any model type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'model_id' => 'model_id',
    'uid' => 'uid',
    'type' => 'type',
    'changed' => 'changed',
    'link_model' => 'link_model',
    'operations' => 'operations',
  );
  $handler->display->display_options['style_options']['default'] = 'model_id';
  $handler->display->display_options['style_options']['info'] = array(
    'model_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'link_model' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['content'] = 'No models have been created yet';
  /* Field: Model: Model ID */
  $handler->display->display_options['fields']['model_id']['id'] = 'model_id';
  $handler->display->display_options['fields']['model_id']['table'] = 'model';
  $handler->display->display_options['fields']['model_id']['field'] = 'model_id';
  /* Field: Model: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'model';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = 'UID';
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  /* Field: Model: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'model';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Model: Changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'model';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['changed']['separator'] = '';
  /* Field: Model: Link */
  $handler->display->display_options['fields']['link_model']['id'] = 'link_model';
  $handler->display->display_options['fields']['link_model']['table'] = 'model';
  $handler->display->display_options['fields']['link_model']['field'] = 'link_model';
  $handler->display->display_options['fields']['link_model']['label'] = 'View';
  /* Field: Model: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'model';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  /* Sort criterion: Model: Model ID */
  $handler->display->display_options['sorts']['model_id']['id'] = 'model_id';
  $handler->display->display_options['sorts']['model_id']['table'] = 'model';
  $handler->display->display_options['sorts']['model_id']['field'] = 'model_id';
  $handler->display->display_options['sorts']['model_id']['order'] = 'DESC';
  /* Filter criterion: Model: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'model';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
  $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'models_admin_page');
  $handler->display->display_options['path'] = 'admin/content/models/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'Models';
  $handler->display->display_options['tab_options']['description'] = 'Manage models';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';


  $translatables['models'] = array(
    t('Master'),
    t('Models'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Empty '),
    t('No models have been created yet'),
    t('Model ID'),
    t('.'),
    t(','),
    t('Name'),
    t('View'),
    t('Operations links'),
    t('Page'),
  );
  $views[] = $view;
  return $views;

}
